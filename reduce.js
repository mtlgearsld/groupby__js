const { name: Name, random, finance } = require('faker');

const students = Array(50)
  .fill('')
  .map(() => ({
    name: `${Name.firstName()} ${Name.lastName()}`,
    age: random.number({ min: 19, max: 28 }),
    gender: 'other',
    gpa: Number(finance.amount(1, 4, 1)),
  }));

const gpaGrp = { gpa1: [], gpa2: [], gpa3: [], gpa4: [] };

const groupByGpa = students.reduce((groups, student) => {
  const base = student.gpa.toString().split('.')[0];
  groups['gpa' + base].push(student);
  return groups;
}, gpaGrp);

for (const student of students) {
  const base = student.gpa.toString().split('.')[0];
  gpaGrp['gpa' + base].push(student);
}

console.log(students);
console.log(groupByGpa);
console.log(gpaGrp);
