var lodash = require('lodash');

const students = [
  { name: 'Jacob', age: 25, gpa: 3.0 },
  { name: 'Isaac', age: 39, gpa: 2.0 },
  { name: 'Heroku', age: 18, gpa: 2.5 },
  { name: 'James', age: 30, gpa: 3.5 },
  { name: 'Taylor', age: 19, gpa: 4.0 },
  { name: 'Marcus', age: 33, gpa: 1.9 },
  { name: 'Max', age: 27, gpa: 1.5 },
  { name: 'Katherine', age: 35, gpa: 2.7 },
  { name: 'Mark', age: 30, gpa: 3.9 },
  { name: 'Sarah', age: 22, gpa: 3.4 },
  { name: 'Lise', age: 18, gpa: 1.2 },
  { name: 'Hanna', age: 19, gpa: 2.6 },
];

groupedGpa = lodash.groupBy(students, function (sortGpa) {
  return sortGpa.gpa >= 1 && sortGpa.gpa <= 2 ? 'Gpa 1-2' : 'Gpa 3-4';
});

groupedAge = lodash.groupBy(students, function (sortAge) {
  return sortAge.age >= 10 && sortAge.age <= 20 ? 'Age 10-20' : 'Age 20-30';
});
console.log(groupedGpa);
console.log(groupedAge);
